#!/usr/bin/env python3

"""
alta3research-requests02

Your script alta3research-requests02.py should demonstrate proficiency with the requests HTTP library.
The API you target is up to you, but be sure any data that is returned is "normalized" 
into a format that is easy for users to understand.

"""

import requests
import pprint

