#!/usr/bin/env python3

"""alta3research-flask01"""

"""Your script alta3research-flask01.py should demonstrate proficiency with the flask library.
Ensure your application has at least two endpoints.
At least one of your endpoints should return legal JSON."""

from flask import Flask
from flask import make_response
from flask import request
from flask import render_template
from flask import redirect
from flask import url_for

app = Flask(__name__)
